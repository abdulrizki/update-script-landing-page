*** Settings ***
Documentation           Landing Page Test Case
Resource                ../Steps/base.robot
Resource                ../pageObject/LandingPage/landingPageKeyword.robot
Resource                ../pageObject/Login/loginDigiportKeyword.robot
Suite Setup             base.Open Firefox Browser
Suite Teardown          Close Browser

*** Test Case ***

User Partner Should Be Able At Landing Page
    [Documentation]                     Test to User Partner should be able at landing page
    [Tags]              Functional
    loginDigiportKeyword.User Partner Should Be Able to Login with Valid Data
    Sleep   5s

User Partner Should Be Able To Upload File Dana from Channel ORBIT
    [Documentation]             Test to User SA should be able to upload file valid
    User click button Upload File
    User verify click upload
    User click Date
    User select Date  
    User click Channel Name
    User select Channel Name ORBIT
    Sleep       10s        
    User choose File dana.csv
    Sleep       10s
    User Click Delimeter    
    User select DelimeterKoma   
    User click Upload
    Sleep       20s
    User click Confirm
    Verify Success to Upload File
    Sleep       8s

# User Partner Should Be Able To Upload File FINNET from Channel ORBIT
#     [Documentation]             Test to User BO should be able to upload file valid
#     User click button Upload File
#     User verify click upload
#     User click Date
#     User select Date
#     User click Channel Name
#     User select Channel Name ORBIT
#     Sleep       10s        
#     User choose File finnet.csv
#     Sleep       20s
#     User Click Delimeter
#     User select Delimeter Quote    
#     User click Upload
#     Sleep       8s
#     User click Confirm
#     Verify Success to Upload File
#     Sleep       8s


# User Partner Should Be Able To Upload File MIDTRANS from Channel ORBIT
#     [Documentation]             Test to User BO should be able to upload file valid
#     User click button Upload File
#     User verify click upload
#     User click Date
#     User select Date
#     User click Channel Name
#     User select Channel Name ORBIT
#     Sleep       10s        
#     User choose File midtrans.xlsx   
#     Sleep       10s 
#     User Click Delimeter
#     User select Delimeter Koma 
#     User click Upload
#     Sleep       25s
#     User click Confirm
#     Sleep       8s
#     Verify Success to Upload File
#     Sleep       10s

# User Partner Should Be Able To Upload File SHOPEE from Channel ORBIT
#     [Documentation]             Test to User BO should be able to upload file valid
#     User click button Upload File
#     User verify click upload
#     User click Date
#     User select Date
#     User click Channel Name
#     User select Channel Name Orbit
#     Sleep       10s        
#     User choose File Xendit-Shopee.csv
#     Sleep       10s
#     User Click Delimeter
#     User select Delimeter Quote
#     User click Upload
#     Sleep       25s
#     User click Confirm
#     Sleep       8s
#     Verify Success to Upload File
#     Sleep       10s

# User Partner Should Be Able To Upload File LINKAJA from Channel ORBIT
#     [Documentation]             Test to User BO should be able to upload file valid
#     User click button Upload File
#     User verify click upload
#     User click Date
#     User select Date
#     User click Channel Name
#     User select Channel Name ORBIT    
#     Sleep       10s   
#     User choose file ovo.csv
#     Sleep       20s
#     User Click Delimeter
#     User select Delimeter Quote
#     User click Upload
#     Sleep       20s
#     User click Confirm
#     Sleep       8s
#     Verify Success to Upload File
#     Sleep       8s

# User Partner Should Be Able Logout at Landing Page
#     [Documentation]                     Test to User Partner should be able logout at landing page
#     [Tags]              Functional

#     loginDigiportKeyword.User Click Logout and Confirm "Ok"